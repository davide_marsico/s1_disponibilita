package com.asw;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



/**
 * This controller gives information about availables fields
 *
 * @author  Davide Marsico

 */


@Configuration
@PropertySource("application.yml")
@RestController
public class AvailabilityController {



	@Value("${ground.groundsHour.uri}")
	private String groundsHourUri;


	@Value("${ground.groundsDay.uri}")
	private String groundsDayUri;

	@Value("${ground.type.uri}")
	private String typeUri;




	@RequestMapping("/{giorno}")
	public String getDisponibilitaGiorno(@PathVariable String giorno) {



		String grounds = getGroundDay();

		String res = getTypes(grounds);

		return "In questa settimana, per " + giorno + ", sono disponibili i seguenti campi: " + res;


	}


	@RequestMapping("/{giorno}/{ora}")
	public String getDisponibilitaGiorno(@PathVariable String giorno, @PathVariable String ora) {


		String grounds = getGroundHour();

		String res = getTypes(grounds);


		return "In questa settimana, per " + giorno + " alle ore " + ora + " , sono disponibili i seguenti campi: " + res;


	}



	private String getGround(String uri) {
		return new RestTemplate().getForObject(uri,String.class);
	}


	private String getType(String uri,String grounds) {
		return new RestTemplate().getForObject(uri+"/"+grounds,String.class);
	}


	private String getTypes(String grounds) {
		return getType(typeUri,grounds);
	}


	private String getGroundHour() {
		return getGround(groundsHourUri);
	}



	private String getGroundDay() {
		return getGround(groundsDayUri);
	}

}
