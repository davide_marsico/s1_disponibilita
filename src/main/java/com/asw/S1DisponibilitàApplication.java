package com.asw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S1DisponibilitàApplication {

	public static void main(String[] args) {
		SpringApplication.run(S1DisponibilitàApplication.class, args);
	}
}
